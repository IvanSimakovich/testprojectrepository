﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FormatConsoleTable
{
    class Program
    {
        static void Main(string[] args)
        {
            Class1 class1 = new Class1(100);

            Console.WriteLine("Case1");
            Console.WriteLine("\tCase1");
            Console.WriteLine("\t1tqwer\t2fasdf\t3gdfg\t4hxcv\t5zxcv\t6asdf");
            Console.WriteLine("\tq\tqq\tqqq\tqqqq\tqqqqq\tqqqqqqq");
            Console.WriteLine("\tww\twww\twwww\twwwww\twwwwww\twwwww");
            Console.WriteLine("\teee\teeee\teeeeee\teeeeee\teeeeeee\teeeeee");
            Console.WriteLine("\trr\trrrrrrr\trrr\trrrrrrr\trrrrrrr\trrrrrrrr");
            Console.WriteLine("\ttttttt\tt\ttt\ttttttt\ttt\tt");

            Console.WriteLine();

            Console.WriteLine("\tTable1");
            Console.WriteLine("\t{0:##.00}\t{1:##.00}\t{2:##.00}\t{3:##.00}", class1.Field1, class1.Field2, class1.Field3, class1.Field4);
            class1 = new Class1(1000);
            Console.WriteLine("\t{0:##.00}\t{1:##.00}\t{2:##.00}\t{3:##.00}", class1.Field1, class1.Field2, class1.Field3, class1.Field4);
            Console.WriteLine("\t\t\t\t{0:##.00}", class1.Total);

            Console.WriteLine();

            Console.WriteLine("Case2");
            Console.WriteLine("{0,14}", "Table2");
            class1 = new Class1(100);
            Console.WriteLine("{0,14}{1,8}{2,8}{3,11}", Math.Round(class1.Field1, 2), Math.Round(class1.Field2, 2), Math.Round(class1.Field3, 2), Math.Round(class1.Field4, 2));
            class1 = new Class1(1000);
            Console.WriteLine("{0,14}{1,8}{2,8}{3,11}", Math.Round(class1.Field1, 2), Math.Round(class1.Field2, 2), Math.Round(class1.Field3, 2), Math.Round(class1.Field4, 2));
            Console.WriteLine("{0}{1,34}", "BUT -> ", Math.Round(class1.Total, 2));

            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine();

            Console.WriteLine("Your Case");
            class1 = new Class1(100);
            Console.WriteLine("{0,14:0000.00}{1,8:0000.00}{2,8:0000.00}{3,11:0000.00}", class1.Field1, class1.Field2, class1.Field3, class1.Field4);
            class1 = new Class1(1000);
            Console.WriteLine("{0,14:#.00}{1,8:#.00}{2,8:#.00}{3,11:#.00}", class1.Field1, class1.Field2, class1.Field3, class1.Field4);
            Console.WriteLine("Total -> {0,32:#.00}", class1.Total);
            Console.WriteLine("OR");
            class1 = new Class1(100);
            Console.WriteLine("\t{0:#.00}\t{1:#.00}\t{2:#.00}\t{3:#.00}", class1.Field1, class1.Field2, class1.Field3, class1.Field4);
            class1 = new Class1(1000);
            Console.WriteLine("\t{0:#.00}\t{1:#.00}\t{2:#.00}\t{3:#.00}", class1.Field1, class1.Field2, class1.Field3, class1.Field4);
            Console.WriteLine("Total ->\t\t\t{0:#.00}", class1.Total);

            Console.ReadKey();
        }
    }
}
