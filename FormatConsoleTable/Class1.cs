﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FormatConsoleTable
{
    public class Class1
    {
        private double _x;
        public Class1(double x)
        {
            _x = x;
        }
        public double Field1 { get { return _x / 3; } }
        public double Field2 { get { return _x / 4; } }
        public double Field3 { get { return _x / 5; } }
        public double Field4 { get { return _x / 6; } }
        public double Total { get { return 100000.10100; } }
    }
}
