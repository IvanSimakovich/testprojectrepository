﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestAbstract
{
    abstract class ClassAbstract
    {
        private string i;

        public abstract string Abs(string x);

        public string NoAbs(string x)
        {
            i = "NON" + x ;
            return i;
        }
    }
}
