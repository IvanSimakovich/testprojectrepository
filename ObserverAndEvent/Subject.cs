﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ObserverAndEvent
{
    internal interface ISubject
    {
        void AddObserver(IObserver observer);
        void RemoveObserver(IObserver observer);
        void NotifyObservers(string s);
    }

    internal class Subject : ISubject
    {
        public string SubjectState { get; set; }
        public List<IObserver> Observers { get; private set; }
        private Simulator simulator;
        private const int speed = 200;

        public Subject()
        {
            Observers = new List<IObserver>();
            simulator = new Simulator();
        }

        public void AddObserver(IObserver observer)
        {
            Observers.Add(observer);
        }

        public void NotifyObservers(string s)
        {
            foreach (var item in Observers)
            {
                item.Update(s);
            }
        }

        public void RemoveObserver(IObserver observer)
        {
            Observers.Remove(observer);
        }
        
        public void Go()
        {
            new Thread(new ThreadStart(Run))
.Start();
        }

        private void Run()
        {
            foreach (string item in simulator)
            {
                Console.WriteLine("Subject: " + item);
                SubjectState = item;
                NotifyObservers(item);
                Thread.Sleep(speed);
            }
        }
    }
}
