﻿using System.Collections;

namespace ObserverAndEvent
{
    internal class Simulator : IEnumerable
    {
        string[] moves = { "5", "3", "1", "6", "7" };

        public IEnumerator GetEnumerator()
        {
            foreach (string item in moves)
            {
                yield return item;
            }
        }
    }
}
