﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObserverAndEvent
{
    internal interface IObserver
    {
        void Update(string state);
    }

    internal class Observer : IObserver
    {
        private string name;
        private ISubject subject;
        private string gap;
        private string state;

        public Observer(ISubject subject, string name,
             string gap)
        {
            this.subject = subject;
            this.name = name;
            this.gap = gap;
            subject.AddObserver(this);
        }

        public void Update(string subjectState)
        {
            state = subjectState;
            Console.WriteLine(gap + name + ": " + state);
        }
    }
}
