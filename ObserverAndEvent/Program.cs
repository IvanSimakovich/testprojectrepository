﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObserverAndEvent
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Subject subject = new Subject();
            Observer observer1 = new Observer(subject, "Center", "\t\t");
            Observer observer2 = new Observer(subject, "Right", "\t\t\t\t");
            subject.Go();

            Console.Read();
        }
    }
}
