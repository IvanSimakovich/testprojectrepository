﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AsyncProject
{
    class Program
    {
        static void Main(string[] args)
        {
            Mainmethod();

            Console.ReadKey();
        }

        static async void Mainmethod()
        {
            Console.WriteLine("Begin");

            await DisplayResultAsync(15);

            Console.WriteLine("First method with 15 is finish");
            DisplayResultAsync(25);

            Console.WriteLine("Second method with 25 is finish");

            DisplayResult();

            Console.WriteLine("Third acync method with 5 is finish");

            Console.ReadLine();
        }

        static async Task DisplayResultAsync(int num)
        {
            string q;
            var x = new StringBuilder();
            int result = await FactorialAsync(num);
            Thread.Sleep(3000);
            q = x.AppendFormat("Async method factorial number {0} equal {1}", num, result).ToString();
            Console.WriteLine(q);
        }


        static Task<int> FactorialAsync(int x)
        {
            int result = 1;

            return Task.Run(() =>
            {
                for (int i = 1; i <= x; i++)
                {
                    result *= i;
                }
                return result;
            });
        }

        static void DisplayResult()
        {
            string q;
            int num = 5;
            var x = new StringBuilder();
            int result = Factorial(num);
            Thread.Sleep(3000);
            q = x.AppendFormat("Simple method factorial number {0} equal {1}", num, result).ToString();
            Console.WriteLine(q);
        }

        static int Factorial(int x)
        {
            int result = 1;

            for (int i = 1; i <= x; i++)
            {
                result *= i;
            }
            return result;
        }
    }
}

