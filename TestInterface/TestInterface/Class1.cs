﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestInterface
{
    class Class1:IChild2
    {
        public bool MethodParent { get; set; }
        public string MethodChild1 { get; set; }
        public int MethodChild2 { get; set; }

        public Class1(bool x)
        {
            MethodParent = x;
        }
        public Class1(string x)
        {
            MethodParent = true;
            MethodChild1 = x;
        }
        public Class1(int x)
        {
            MethodParent = true;
            MethodChild1 = "3";
            MethodChild2 = x;
        }

        public Class1(int x, string y, bool z)
        {
            MethodParent = z;
            MethodChild1 = y;
            MethodChild2 = x;
        }

    }
}
