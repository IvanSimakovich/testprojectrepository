﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestInterface
{
    interface IChild2:IChild1
    {
        int MethodChild2 { get; set; }
    }
}
