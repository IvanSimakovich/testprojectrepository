﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestInterface
{
    interface IChild1 : IParent
    {
        string MethodChild1 { get; set; }
    }
}
