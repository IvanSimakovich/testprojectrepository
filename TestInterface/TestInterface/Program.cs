﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace TestInterface
{
    class Program
    {
        static void Main(string[] args)
        {
            SecondExample();

            Console.ReadKey();
        }

        private static void FirstExample()
        {


            IChild2 ichild2 = new Class1(3);
            Console.WriteLine("iChild2 = {0}", ichild2.MethodParent.ToString());
            Console.WriteLine("iChild2 = {0}", ichild2.MethodChild1.ToString());
            Console.WriteLine("iChild2 = {0}", ichild2.MethodChild2.ToString());

            Console.WriteLine();

            IChild1 ichild1 = new Class1("2");
            Console.WriteLine("iChild1 = {0}", ichild1.MethodParent.ToString());
            Console.WriteLine("iChild1 = {0}", ichild1.MethodChild1.ToString());
            Console.WriteLine();

            IParent iparent = new Class1(false);
            Console.WriteLine("iparent = {0}", iparent.MethodParent.ToString());
            Console.WriteLine();

            Console.WriteLine("iChild2 = {0}", ichild2.MethodParent.ToString());
            Console.WriteLine("iChild2 = {0}", ichild2.MethodChild1.ToString());
            Console.WriteLine("iChild2 = {0}", ichild2.MethodChild2.ToString());
            Console.WriteLine();

            Class1 class1 = new Class1(8, "9", true);
            Console.WriteLine("class1.parent = {0}", class1.MethodParent.ToString());
            Console.WriteLine("class1.child1 = {0}", class1.MethodChild1.ToString());
            Console.WriteLine("class1.Child2 = {0}", class1.MethodChild2.ToString());
            Console.WriteLine();


            Console.WriteLine("iChild2 = {0}", ichild2.MethodParent.ToString());
            Console.WriteLine("iChild2 = {0}", ichild2.MethodChild1.ToString());
            Console.WriteLine("iChild2 = {0}", ichild2.MethodChild2.ToString());
            Console.WriteLine();

            Console.WriteLine("iparent = {0}", iparent.MethodParent.ToString());
            Console.WriteLine();

            ichild2 = class1;
            Console.WriteLine("//////////////ichild2 = class1");
            Console.WriteLine("iChild2 = {0}", ichild2.MethodParent.ToString());
            Console.WriteLine("iChild2 = {0}", ichild2.MethodChild1.ToString());
            Console.WriteLine("iChild2 = {0}", ichild2.MethodChild2.ToString());
            Console.WriteLine();

            ichild1 = class1;
            Console.WriteLine("//////////////ichild1 = class1");
            Console.WriteLine("iChild1 = {0}", ichild1.MethodParent.ToString());
            Console.WriteLine("iChild1 = {0}", ichild1.MethodChild1.ToString());
            Console.WriteLine();

            class1.MethodChild1 = "Hello worl";

            Console.WriteLine("//////////////class1.MethodChild1 = 'Hello worl'");
            Console.WriteLine("iChild2 = {0}", ichild2.MethodParent.ToString());
            Console.WriteLine("iChild2 = {0}", ichild2.MethodChild1.ToString());
            Console.WriteLine("iChild2 = {0}", ichild2.MethodChild2.ToString());
            Console.WriteLine();

            Console.WriteLine("//////////////class1.MethodChild1 = 'Hello worl'");
            Console.WriteLine("iChild1 = {0}", ichild1.MethodChild1.ToString());
            Console.WriteLine();
        }

        private static void SecondExample()
        {
            Class1 class1 = new Class1(1, "1", true);
            Console.WriteLine("//////////////class1.MethodChild1 = '1'");
            Console.WriteLine("class1 = {0}", class1.MethodParent.ToString());
            Console.WriteLine("class1 = {0}", class1.MethodChild1.ToString());
            Console.WriteLine("class1 = {0}", class1.MethodChild2.ToString());
            Console.WriteLine();

            IChild1 iChild1 = class1;
            iChild1.MethodChild1 = "iChild1";
            iChild1.MethodParent = false;
            Console.WriteLine("//////////////iChild1.MethodChild1 = '1'");
            Console.WriteLine("iChild1 = {0}", iChild1.MethodParent.ToString());
            Console.WriteLine("iChild1 = {0}", iChild1.MethodChild1.ToString());
            Console.WriteLine();

            Console.WriteLine("//////////////class1.MethodChild1 = '1'");
            Console.WriteLine("class1 = {0}", class1.MethodParent.ToString());
            Console.WriteLine("class1 = {0}", class1.MethodChild1.ToString());
            Console.WriteLine("class1 = {0}", class1.MethodChild2.ToString());
            Console.WriteLine();


            IChild2 iChild2 = class1;
            iChild2.MethodChild2 = 3;
            iChild2.MethodChild1 = "iChild2";
            iChild2.MethodParent = false;
            Console.WriteLine("//////////////iChild2.MethodChild1 = '1'");
            Console.WriteLine("iChild2 = {0}", iChild2.MethodParent.ToString());
            Console.WriteLine("iChild2 = {0}", iChild2.MethodChild1.ToString());
            Console.WriteLine("iChild2 = {0}", iChild2.MethodChild2.ToString());
            Console.WriteLine();

            Console.WriteLine("//////////////class1.MethodChild1 = '1'");
            Console.WriteLine("class1 = {0}", class1.MethodParent.ToString());
            Console.WriteLine("class1 = {0}", class1.MethodChild1.ToString());
            Console.WriteLine("class1 = {0}", class1.MethodChild2.ToString());
            Console.WriteLine();

        }
    }
}
